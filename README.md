[![pipeline status](https://forge.univ-lyon1.fr/p2101833/userAPI/badges/master/pipeline.svg)](https://forge.univ-lyon1.fr/p2101833/userAPI/-/commits/master)

# User API Angular

## Auhtor

-   Name: PUPIER
-   First Name: Loïc
-   Class: RA2

## Description

Ceci est une application Angular qui permet de récupérer des informations sur des utilisateurs via une API REST Mock.

-   Lien vers l'application: [GitLab Pages](http://userapi-p2101833-edb550edccb305817ec986bc6e3ad4d080eadb988f46ed.pages.univ-lyon1.fr/)

-   Lien vers l'api Mock: [Mock API](https://6583324202f747c8367b3da4.mockapi.io/api/v1/user)

Credentials:

    Login: admin
    Password: admin

## Development server

Run `npm install` to install all the dependencies.
Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.
The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

export class User {
	static nextId: number = 0;
	id: number = User.nextId++;
	name!: string;
	desc!: string;
	email!: string;
	job!: string;

	constructor(name: string, desc: string, email: string, job: string) {
		this.name = name;
		this.desc = desc;
		this.email = email;
		this.job = job;
	}

	equals(user: User): boolean {
		return (
			this.name === user.name &&
			this.desc === user.desc &&
			this.email === user.email &&
			this.job === user.job
		);
	}
}

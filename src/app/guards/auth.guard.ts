import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from '../pages/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

export const authGuard: CanActivateFn = (route, state) => {
	const router = inject(Router);
	const auth = inject(AuthService);
	const _snackBar = inject(MatSnackBar);
	if (!auth.isAuthenticated()) {
		router.navigateByUrl('/login');
		_snackBar.open('Vous devez être connecté !');
		return false;
	}
	return true;
};

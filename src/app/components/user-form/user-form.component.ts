import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/components/user/user.service';

@Component({
	selector: 'app-user-form',
	templateUrl: './user-form.component.html',
	styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
	form: FormGroup = new FormGroup({});
	@Input() user?: User;

	constructor(
		private userHttpService: UserService,
		private router: Router,
		private _snackBar: MatSnackBar,
		public fb: FormBuilder
	) {}
	ngOnInit(): void {
		this.form = this.fb.group({
			name: [this.user?.name, Validators.required],
			desc: [this.user?.desc, Validators.required],
			email: [this.user?.email, [Validators.email, Validators.required]],
			job: [this.user?.job, Validators.required],
		});
	}

	submit(): void {
		if (this.user) {
			const tempUser = {
				...this.user,
				...this.form.value,
			};
			this.userHttpService.updateUser(tempUser).subscribe(() => {
				this._snackBar.open('Utilisateur mis a jour avec succès !');
				this.router.navigateByUrl('/home');
			});
		} else {
			const newUser = this.form.value as User;
			this.userHttpService.addUser(newUser).subscribe(() => {
				this._snackBar.open('Utilisateur ajouté avec succès !');
				this.router.navigateByUrl('/home');
			});
		}
	}
}

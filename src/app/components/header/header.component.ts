import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/pages/auth/auth.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
	constructor(public authService: AuthService, public router: Router) {}

	navigate() {
		if (this.authService.isAuthenticated()) {
			this.authService.logout();
			this.router.navigateByUrl('/home');
		} else {
			this.router.navigateByUrl('/login');
		}
	}
}

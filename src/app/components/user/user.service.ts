import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { Properties } from '../../app.properties';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	baseUrl = Properties.API_URL;

	constructor(private http: HttpClient) {}

	public getUsers() {
		return this.http.get<User[]>(this.baseUrl);
	}

	public getUser(id: string) {
		return this.http.get<User>(`${this.baseUrl}/${id}`);
	}

	public updateUser(user: User) {
		return this.http.put(`${this.baseUrl}/${user.id}`, user);
	}

	public addUser(user: User) {
		return this.http.post(this.baseUrl, user);
	}

	public deleteUser(user: User) {
		return this.http.delete(`${this.baseUrl}/${user.id}`);
	}
}

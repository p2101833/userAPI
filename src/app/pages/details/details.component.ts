import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/components/user/user.service';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
	user?: User;

	constructor(
		public activatedRoute: ActivatedRoute,
		public router: Router,
		private UserHttpService: UserService
	) {}

	ngOnInit(): void {
		const id = this.activatedRoute.snapshot.params['id'];
		this.UserHttpService.getUser(id).subscribe((user: User) => {
			this.user = user;
		});
	}
}

import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/pages/auth/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements AfterViewInit {
	@ViewChild('loginInput') loginInput: any;
	form: FormGroup;

	constructor(
		private authService: AuthService,
		private fb: FormBuilder,
		private router: Router,
		private _snackbar: MatSnackBar
	) {
		this.form = this.fb.group({
			login: ['', Validators.required],
			password: ['', Validators.required],
		});
	}

	ngAfterViewInit() {
		this.loginInput.nativeElement.focus(); // Ajoutez cette ligne
	}

	keyPress(event: KeyboardEvent) {
		if (event.key === 'Enter') {
			this.submit();
		}
	}

	submit() {
		if (this.authService.login(this.form.value)) {
			this.router.navigateByUrl('/home');
		}
	}
}

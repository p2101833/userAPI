import { Injectable } from '@angular/core';
import Credentials from '../../models/credentials.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	private authentified = false;
	private timer = 0;

	constructor(private _snackbar: MatSnackBar) {}

	login(credentials: Credentials) {
		const { login, password } = credentials;

		if (login === 'admin' && password === 'admin' && this.timer === 0) {
			this.authentified = true;
			this._snackbar.open('Vous êtes maintenant connecté !');
		} else {
			if (this.timer === 0) {
				this.timer = 10;
				const intervalId = setInterval(() => {
					console.log(this.timer);
					this.timer--;
					if (this.timer === 0) {
						clearInterval(intervalId);
					}
				}, 1000);
			}
			const errorMessage = `La combinaison login / mot de passe est erronée ! Attendez ${this.timer} secondes avant de réessayer.`;
			this._snackbar.open(errorMessage);
			this.authentified = false;
		}
		return this.authentified;
	}

	logout() {
		this.authentified = false;
		this._snackbar.open('Vous êtes maintenant déconnecté !');
	}

	isAuthenticated(): boolean {
		return this.authentified;
	}
}

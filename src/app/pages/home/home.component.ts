import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/pages/auth/auth.service';
import { UserService } from 'src/app/components/user/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit {
	isLoadingResults = true;
	isFilter = false;
	filterForm: FormGroup = new FormGroup({});
	displayedColumns: string[] = ['id', 'name', 'job', 'email', 'actions'];
	dataSource = new MatTableDataSource<User>([]);

	@ViewChild(MatPaginator) paginator!: MatPaginator;
	@ViewChild(MatSort) sort!: MatSort;

	constructor(
		public userHttpService: UserService,
		public authService: AuthService,
		private fb: FormBuilder,
		private router: Router
	) {}

	ngOnInit(): void {
		this.filterForm = this.fb.group({
			name: [''],
			job: [''],
			email: [''],
		});

		this.filterForm.valueChanges.subscribe(() => {
			this.dataSource.filter = this.filterForm.value;
		});

		this.dataSource.filterPredicate = (record, filter: any) => {
			const { name, job, email } = filter;

			const nameMatch = record.name
				.toLowerCase()
				.includes(name.toLowerCase());
			const jobMatch = record.job
				.toLowerCase()
				.includes(job.toLowerCase());
			const emailMatch = record.email
				.toLowerCase()
				.includes(email.toLowerCase());

			return nameMatch && jobMatch && emailMatch;
		};
	}

	ngAfterViewInit(): void {
		this.userHttpService.getUsers().subscribe((users) => {
			this.dataSource.data = users;
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
			this.isLoadingResults = false;
		});
	}

	delete(user: User) {
		if (confirm(`Etes vous sûr de vouloir supprimer ${user.name} ?`)) {
			this.userHttpService.deleteUser(user).subscribe(() => {
				this.dataSource.data = this.dataSource.data.filter(
					(u) => u.id != user.id
				);
			});
		}
	}

	toggleFilter() {
		this.isFilter = !this.isFilter;
	}

	addUser() {
		if (!this.authService.isAuthenticated()) {
			this.router.navigate(['/login']);
		}
		this.router.navigate(['/add']);
	}
}
